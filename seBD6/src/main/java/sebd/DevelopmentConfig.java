package sebd;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.crypto.password.PasswordEncoder;
import sebd.data.UserRepository;
import sebd.data.WeedRepository;

@Profile("!prod")
@Configuration
public class DevelopmentConfig {

    @Bean
    public CommandLineRunner dataLoader(WeedRepository weedRepo, UserRepository userRepo, PasswordEncoder encoder) { // user repo for ease of testing with a built-in user
        return args -> {
            weedRepo.deleteAll();
            weedRepo.save(new Weed("weed1", 5));
            weedRepo.save(new Weed("weed2", 6));

            userRepo.deleteAll();
            userRepo.save(new User("test", encoder.encode("test")));
        };
    }

}
