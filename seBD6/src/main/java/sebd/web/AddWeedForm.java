package sebd.web;

import lombok.Data;
import sebd.Weed;

@Data
public class AddWeedForm {

    private String name;
    private Integer weeksTilGrown;

    public Weed toWeed() {
        return new Weed(name, weeksTilGrown);
    }

}
