package sebd.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import sebd.data.WeedRepository;

@Controller
@RequestMapping("/manage")
@SessionAttributes("weed")
public class ManageController {

    private final WeedRepository weedRepo;
    private WeedProps props;

    @Autowired
    public ManageController(WeedRepository weedRepo, WeedProps weedProps) {
        this.weedRepo = weedRepo;
        this.props = weedProps;
    }

    @GetMapping
    public String weeds(Model model) {
        Pageable pageable = PageRequest.of(0, props.getPageSize());
        model.addAttribute("weeds", weedRepo.findAll(pageable));
        return "manage";
    }

    @PostMapping("/add")
    public String processRegistration(AddWeedForm form) {
        weedRepo.save(form.toWeed());
        return "redirect:/manage";
    }

    @PostMapping("/delete")
    public String delete(@RequestParam(value = "id") Long id) {
        weedRepo.deleteById(id);
        return "redirect:/manage";
    }
}
