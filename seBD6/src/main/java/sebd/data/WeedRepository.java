package sebd.data;

import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import sebd.Weed;

import java.util.List;

public interface WeedRepository
        extends CrudRepository<Weed, Long> {
    List<Weed> findAll(Pageable pageable);

    void deleteById(Long id);
}
