package sebd.data;

import org.springframework.data.repository.CrudRepository;
import sebd.User;

public interface UserRepository extends CrudRepository<User, Long> {

    User findByUsername(String username);

}
